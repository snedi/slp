<?php
/**
 * Created by PhpStorm.
 * User: snedi
 * Date: 04.11.2015
 * Time: 15:58
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="channels-item">
    <h4><?= Html::a($model['name'],['site/show-channel', 'chan_id' => $model['id'], 'chan_type' => $model['type'], 'chan_name' => $model['name']],['class' => 'btn btn-info btn-block btn-channels']) ?></h4>
</div>