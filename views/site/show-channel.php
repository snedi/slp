<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $chan_name;
$this->params['breadcrumbs'][] = ['label' => 'Другие каналы', 'url' => ['site/soc-tv']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div>
        <iframe width="480" height="270" src="https://www.ustream.tv/embed/<?= $chan_id ?>?html5ui" allowfullscreen webkitallowfullscreen scrolling="no" frameborder="0" style="border: 0 none transparent;"></iframe>
    </div>
</div>
