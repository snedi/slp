<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Другие каналы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_channels_item',
            'layout' => '{items}'
        ]);
    ?>
</div>
