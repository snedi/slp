<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\helpers\StreamDataHelper;

$this->title = 'Прямая трансляция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>На данный момент трансляций запусков нет</p>
</div>
