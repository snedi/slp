<?php
/**
 * Created by PhpStorm.
 * User: snedi
 * Date: 04.11.2015
 * Time: 15:01
 */
namespace app\helpers;

class StreamDataHelper {

    const USTREAM = 1;
    const YOUTUBE = 2;

    public static $ustream = [
        ['id' => '6540154', 'name' => 'NASA Public', 'type' => self::USTREAM],
        ['id' => '17074538', 'name' => 'ISS HD EVE', 'type' => self::USTREAM],
        ['id' => '9408562', 'name' => 'Live ISS Stream', 'type' => self::USTREAM],
    ];
}